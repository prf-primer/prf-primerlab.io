# Market Structure Primer
Documentation website built with [VuePress](https://vuepress.vuejs.org/).

[![pipeline status](https://gitlab.com/prf-primer/prf-primer.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/prf-primer/prf-primer.gitlab.io/-/commits/master)


<!---------------------------------->
## I. Quick Start
<!---------------------------------->

### Local Development

1. Install dependencies.
	```sh
	$ npm install
	```
2. Start vuewpress dev server.
	```sh
	$ npm run dev
	```
3. Go to [localhost:8080](http://localhost:8080).


#### Making Edits

- Content files are located in project folders under [/docs][link-docs].
Inside a project, each folder represents a sub-directory on the production site under the same namespace.
- Contents inside `README.md` are rendered as page contents.
You can modify and preview these files directly in Gitlab's web editor.
- Please refer to the [example project][link-example] for Kramdown syntax and features.
- The [Vuepress config file][link-config] contains the parameters for the sidebar.
- If you want to create a new page, change an existing page’s name or the way its labeled, just make sure those changes are also captured in config.js for the change to be reflected in the sidebar


### Deploying to Production

1. Commit your changes, push or merge to `master`. Any commit added to `master` will trigger a build and release into production.
2. See your changes on [https://primer.prooftrading.com](https://primer.prooftrading.com).



<!---------------------------------->
## II. Project Information
<!---------------------------------->

### YAML front matter

YFM is a YAML snippet placed at the top of a Markdown document that contains metadata for the page and its contents.

YFM is optional. A valid YAML snippet must have the following:

1. Set at the very top of the Markdown document.
1. Begin and end with `---`.
1. At least one directive.

Example:
```yaml
---
home: true
---
```



[link-docs]: https://gitlab.com/prf-docs/prf-docs.gitlab.io/tree/master/docs
[link-example]: https://gitlab.com/prf-docs/prf-docs.gitlab.io/tree/master/docs/example-doc
[link-config]: https://gitlab.com/prf-primer/prf-primer.gitlab.io/blob/master/docs/.vuepress/config.js
