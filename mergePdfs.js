const PDFMerge = require('easy-pdf-merge')
const path = require('path')
const fs = require('fs')


// Map out path to folder containing generated pdf...
const PDF_FOLDER_PATH = process.env.npm_package_config_pdfFolderPath
const projPath = path.resolve(__dirname)
const pdfFolderPath = `${projPath}/${PDF_FOLDER_PATH}`
console.log('pdfFolderPath: ', pdfFolderPath)

function fullPath(fileName){
  return `${pdfFolderPath}/${fileName}`
}

generatePDF()

async function generatePDF(){

  let pdfFilePathsAllFlat = []
  const mapGroupToPages = {
    // '03': ['03.00-participants-.pdf', '03.01-participants-issuers.pdf', ... ]
  }
  await new Promise((resolve)=>{
    fs.readdir(pdfFolderPath, (err, files) => {
      if (err) throw err;
      console.log(`+ + + read ${files.length} files from pdf folder + + +`)
      let homePageName
      files.forEach((fileName, i) => {

        const fileNameParts = fileName.replace('.pdf','').split('_')
        const [grpIndex, pageIndex] = fileNameParts[0].split('.')
        let grpName = fileNameParts[1]
        if (grpName === '') grpName = 'site-home'
        console.log(`grpName: '${grpName}' - grpIndex: ${grpIndex} - pageIndex: ${pageIndex}`)

        if (i === 0) { // site home page: 00.00 --> '00.00-.pdf' 
          homePageName = fileName
        } else {
          const grpFileName = `section_${grpIndex}_${grpName}.pdf`
          // do not put home page under a group...
          if (mapGroupToPages[grpFileName] === undefined) { // new page group...
            mapGroupToPages[grpFileName] = [fullPath(homePageName), fullPath(fileName)] // include home page as cover page
          } else { // append under page group...
            mapGroupToPages[grpFileName].push(fullPath(fileName))
          }
        }        

        pdfFilePathsAllFlat.push(fullPath(fileName))

      })
      console.log('mapGroupToPages -->',mapGroupToPages)
      resolve()
    })
  })
    
  for (const grpFileName in mapGroupToPages) {
    const grpPagePaths = mapGroupToPages[grpFileName]
    // Document per Page Group...
    await new Promise(resolve => {
      PDFMerge(grpPagePaths, fullPath(grpFileName), err => {
        if (err) throw err
        console.log(`Export ${fullPath(grpFileName)} file!`)
        resolve()
      })
    })
  }


  // Full Document...
  const fullDocName = `${pdfFolderPath}/Proof-Market-Structure-Primer.pdf`
  await new Promise(resolve => {
    PDFMerge(pdfFilePathsAllFlat, fullDocName, err => {
      if (err) throw err
      console.log(`Export ${fullDocName} file!`)
      resolve()
    })
  })
}
