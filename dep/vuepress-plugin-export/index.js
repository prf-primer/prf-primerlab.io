const puppeteer = require('puppeteer') // https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md
// const PDFMerge = require('easy-pdf-merge')
const { join } = require('path')
const { dev } = require('vuepress')
const { fs, logger, chalk, path } = require('@vuepress/shared-utils')
const { red, yellow, gray } = chalk

const PDF_FOLDER_PATH = process.env.npm_package_config_pdfFolderPath
const { swapSeparators, removeDirFiles, stringifyIndex, trimOffHtmlExt } = require('../../util')


// Keep silent before running custom command.
logger.setOptions({ logLevel: 1 })

module.exports = (opts = {}, ctx) => ({
  name: 'vuepress-plugin-export',

  chainWebpack(config) {
    config.plugins.delete('bar')
    // TODO vuepress should give plugin the ability to remove this plugin
    config.plugins.delete('vuepress-log')
  },

  extendCli(cli) {
    cli
      .command('export [targetDir]', 'export current vuepress site to a PDF file')
      .allowUnknownOptions()
      .action(async (dir = '.') => {
        dir = join(process.cwd(), dir)
        try {
          const nCtx = await dev({
            sourceDir: dir,
            clearScreen: false,
            theme: opts.theme || '@vuepress/default',
            siteConfig: {
              themeConfig:{
                sidebar: false,
                navbar: false,
              }
            }
          })
          logger.setOptions({ logLevel: 3 })
          logger.info(`Start to generate current site to PDF ...`)
          try {
            await generatePDF(nCtx, {
              port: nCtx.devProcess.port,
              host: nCtx.devProcess.host,
              sortedPageMap: opts.sortedPageMap,
            })
          } catch (error) {
            console.error(red(error))
          }
          nCtx.devProcess.server.close()
          process.exit(0)
        } catch (e) {
          throw e
        }
      })
  }
})

async function generatePDF(ctx, {
  port,
  host,
  sortedPageMap,
}) {
  const { pages, tempPath, siteConfig, isProd, sourceDir } = ctx
  const tempDir = join(tempPath, 'pdf')


  let exportPages = pages.slice(0)


  logger.success('-------tempDir--------')
  logger.success(tempDir)
  logger.success('-------__dirname--------')
  logger.success(path.resolve(__dirname))
  logger.success('-------isProd--------')
  logger.success(isProd)
  logger.success('-------sourceDir--------')
  logger.success(sourceDir)
  logger.success('-------rootDir--------')
  const rootDir = sourceDir.replace('/docs', '')
  logger.success(sourceDir)

  // fs.ensureDirSync(tempDir)
  fs.ensureDirSync(`${rootDir}/${PDF_FOLDER_PATH}/`)
  await removeDirFiles(`${rootDir}/${PDF_FOLDER_PATH}/`) // clear public folder of previous pdfs...

  // console.log(sortedPageMap)
  // const sortedPageMap = {
  //   '/': '00.00',
  //   '/introduction/': '01.00',
  //   '/lifecycle-of-order/': '02.00',
  //   '/participants/': '03.00',
  // }

  exportPages = exportPages.map(page => {
    const url = trimOffHtmlExt(page.path) // --> '/participants/', '/participants/brokers' etc...
    const lowDashedUrl = swapSeparators(url, '_') // --> '_participants_', '_participants_brokers' etc...
    const strPageIndex = sortedPageMap[url] // '00.00', '01.01' etc...
    const fileName = `${strPageIndex}${lowDashedUrl}`
    return {      
      title: page.title,
      location: `http://${host}:${port}${page.path}`,
      pageIndex: strPageIndex,
      puppeteerExportPath: `${rootDir}/${PDF_FOLDER_PATH}/${fileName}.pdf`
      // path: `${tempDir}/${page.key}.pdf`
    }
  })

  exportPages.sort((A, B) => Number(A.pageIndex) - Number(B.pageIndex)) // sort by string index...


  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--headless']
  })
  const browserPage = await browser.newPage()
  
  logger.info(`${yellow('+ + + + + Converting HTML to PDFs + + + + +')}`)
  console.log('process.env.NODE_ENV:', process.env.NODE_ENV)

  for (let i = 0; i < exportPages.length; i++) {
    const {
      location,
      title,
      puppeteerExportPath,
    } = exportPages[i]
    await browserPage.goto(
      location,
      { waitUntil: 'networkidle2' }
    )
    await browserPage.pdf({
      // https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#pagepdfoptions
      path: puppeteerExportPath,
      format: 'Letter',
      displayHeaderFooter: true,
      // 'date' formatted print date
      // 'title' document title
      // 'url' document location
      // 'pageNumber' current page number
      // 'totalPages' total pages in the document
      headerTemplate: `
        <div style="padding-left: 62px; color: #ddd; font-size:9px; font-family: Arial;">
          <span class="date style="margin-right: 8px;"></span>
          <span>primer.prooftrading.com</span>
        </<div>
      `,
      footerTemplate: '<div/>', // placeholder here to prevent default footer from printing
      margin:{
        top: '75px',
        bottom: '75px',
        right: '50px',
        left: '50px',
      }
    })
    logger.success(`Generated ${yellow(puppeteerExportPath)}`)
  }

  await browser.close()
  fs.removeSync(tempDir)
}


