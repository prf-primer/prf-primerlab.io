const fs = require('fs')
const path = require('path')

function stringifyIndex(index) {
  if (index < 10) {
    return '0' + String(index)
  } else {
    return String(index)
  }
}


function swapSeparators(path, sep){
  return path.split('/').join(sep)
}

function trimOffHtmlExt(path) {
  return path.replace('.html', '')
}

function removeDirFiles(directory) {
  fs.readdir(directory, (err, files) => {
    if (err) throw err;
    console.log(`removing ${files.length} files from ${directory}...`)
    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
        console.log(`removing file: ${file}`)
      });
    }
  })
}



function moveFile(oldPath, newPath, callback) {
  fs.rename(oldPath, newPath, function (err) {
    if (err) {
      if (err.code === 'EXDEV') {
        copy();
      } else {
        callback(err);
      }
      return;
    }
    callback();
  });

  function copy() {
    var readStream = fs.createReadStream(oldPath);
    var writeStream = fs.createWriteStream(newPath);

    readStream.on('error', callback);
    writeStream.on('error', callback);

    readStream.on('close', function () {
      fs.unlink(oldPath, callback);
    });

    readStream.pipe(writeStream);
  }
}




module.exports = {
  swapSeparators,
  removeDirFiles,
  stringifyIndex,
  moveFile,
  trimOffHtmlExt,
}

