# Glossary

### Alpha
in the context of trading, when an order has alpha, it means it has inherent profitability that has not yet been realized. For example, if the market is about to move higher, a buy order placed at the current market price has alpha. Conversely, an order with negative alpha would

### Bid

### Offer

### BBO

### NBBO

### Passive (order)
when referring to an order, the term *passive* means priced conservatively relative to the prevailing market. For example, if the prevailing best offer for a stock is $10.00 (i.e. the lowest price at which someone is advertising publicly their desire to sell the stock is $10), a passive buy order would be one priced *lower* than $10, such that it would not immediately transact in the market.

### Aggressive (order)
when referring to an order, the term *aggressive* means the order is priced at a level such that it can immediately interact with a counterparty. For example, if the prevailing best offer for a stock is $10.00 (i.e. the lowest price at which someone is advertising publicly their desire to sell the stock is $10), an aggressive buy order would be one priced *at or higher* than $10, such that it can immediately transact in the market.

### Displayed (order)
a *displayed* order is one whose price and size are publicly disseminated on a given exchange or ECN's market data feed.

### Non-displayed (order)
a *non-displayed* or *hidden* order is one that is live and available to transact on a venue, but that is not advertised via the venue's market data feed.

### Direct bilateral relationship 
a "bilateral relationship" is simply a relationship between two parties, in this case a retail broker and a retail wholesaler.