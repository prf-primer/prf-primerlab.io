

# The Life Cycle of An Order/Trade

This graphic illustrates how the various participants in the stock market interact to form the lifecycle of an order placed by an institutional investor, from its inception through to any trades that result. At the periphery of the process (outside of the stock market itself), end investors give money to an investment manager. The portfolio manager decides what stocks to buy and sell, and in what quantities. When the portfolio manager decides to buy/sell a stock, they communicate this to a trader, typically through a piece of technology known as an order management system (OMS). The trader makes high level decisions about how the trade(s) should be accomplished, like what broker(s) should be used and what algorithms. The trader's instructions are then communicated to a broker-dealer, typically through a piece of technology known as an execution management system (EMS). The broker consumes real-time market data from various sources, and uses this to make low level decisions about how best to implement the instructions in current market conditions. The broker produces "child" orders derived from the "parent" order instructions, and routes them to trading venues such as exchanges and dark pools. On these venues, the child orders may interact (either immediately or after some waiting) with orders submitted by counterparties like themselves or by proprietary traders, so-called because they trade on their own behalf, not on behalf of external end investors. When two orders on a venue are matched in a trade, the venue sends the relevant information back to the brokers/proprietary traders, and also reports it to the "tape," meaning that it becomes part of the market data that others consume.

<object id="svg-object" data="/assets/img/lifecycle_of_order.svg" type="image/svg+xml"></object>



<DownloadPageAsPdf/>



<script>
  // https://vuepress.vuejs.org/guide/using-vue.html
  export default {
    mounted () {
      import('./svgDiagramEnhancer').then(module => {
        const svgDiagramEnhancer = module.default
        svgDiagramEnhancer.enhance('svg-object')
      })
    }
  }
</script>

