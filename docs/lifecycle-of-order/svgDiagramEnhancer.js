const PARTICIPANTS = {
  'INVESTOR': '/participants/',
  'PM': '/participants/investors.html',
  'TRADER': '/participants/investors.html',
  'BROKER': '/participants/brokers.html',
  'EXCHANGE': '/participants/venues.html#exchanges',
  'DARK_POOL': '/participants/venues.html#dark-pools',
  'PROP': '/participants/prop.html',
  'VENDOR': '/participants/vendors.html',
  'OMS': '/when-how/comm-investors-brokers.html#ems-oms-providers',
  'EMS': '/when-how/comm-investors-brokers.html#ems-oms-providers',
}


function scaleW(w, h, newW){
  return {
    w: newW,
    h: h/w * newW,
  }
}

function getPxVal(string){
  return Number(string.split('px')[0])
}

export default {
  enhance(svgObjectId) {
    const el_content = document.body.getElementsByClassName('theme-default-content')[0]
    const contentStyle = window.getComputedStyle(el_content)
    const contentWidth = contentStyle.getPropertyValue('width')

    document.getElementById(svgObjectId).addEventListener('load', function () {
      const svgDoc = this.getSVGDocument() // get the SVG document inside the <object> tag  --> https://developer.mozilla.org/en-US/docs/Web/SVG/Scripting#Inter-document_scripting:_referencing_embedded_SVG
      
      //0. Scale SVG to match content width via VuePress default theme 'content' element...
      const el_svg = svgDoc.getElementById('svg-element')
      const svgWidth = el_svg.getAttribute('width')
      const svgHeight = el_svg.getAttribute('height')
      if (svgWidth !== contentWidth){
        const numContentW = getPxVal(contentWidth)
        const numSvgW = getPxVal(svgWidth)
        const numSvgH = getPxVal(svgHeight)
        const { w, h } = scaleW(numSvgW, numSvgH, numContentW )
        el_svg.setAttributeNS(null, 'width', `${w}px`)
        el_svg.setAttributeNS(null, 'height', `${h}px`)
      }
      
      // 1. Set up mouse over shapes...
      Object.keys(PARTICIPANTS).reduce((prev, participantKey)=>{
        const pathToDoc = PARTICIPANTS[participantKey]
        const selectorShape = svgDoc.getElementById(participantKey)
        selectorShape.setAttributeNS(null, 'class', 'selector-shape')
        selectorShape.addEventListener('click', () => {
          window.location.href = pathToDoc // go to associated documentation page
        })
      }, {})
      
      // 2. Set up dash line animations...
      const groupForwardLines = svgDoc.getElementById('DASHED_LINE_FORWARD')
      const groupBackwardLines = svgDoc.getElementById('DASHED_LINE_BACKWARD');
      [
        ...groupForwardLines.getElementsByTagName('line'),
        ...groupForwardLines.getElementsByTagName('path'),
      ].forEach((svgLine)=>{
        svgLine.setAttributeNS(null, 'class', 'anim-path-forward')
      });
      [
        ...groupBackwardLines.getElementsByTagName('line'),
        ...groupBackwardLines.getElementsByTagName('path'),
      ].forEach((svgLine)=>{
        svgLine.setAttributeNS(null, 'class', 'anim-path-backward')
      });

    })
  }
}



