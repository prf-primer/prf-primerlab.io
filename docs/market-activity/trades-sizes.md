# Trades and Sizes

On any given day, around 6.7 billion shares are traded in the US equities market. ETFs represent about 18-20% of these volumes. Trades typically happen in round "lots," which for most stocks are multiples of 100 shares. The average trade size is currently very small - roughly 200 shares per trade. This is not to say that large trades aren't also common. Block trades (trades of at least 10,000 shares) happen frequently. For example, looking at the [Nasdaq daily market summary](http://www.nasdaqtrader.com/Trader.aspx?id=DailyMarketSummary) of all trading in Nasdaq-listed securities for June 7, 2019, we see that there were over 8000 block trades, accounting for close to 20% of the day's total trading volume. The 200 share average trade size hides a much more varied distribution, comprised of many large trades, many small trades of 200 or 100 shares, and some even smaller "odd lot" trades of less than 100 shares. 

Since there are roughly 6.7 billion shares traded each day with an average trade size of roughly 200 shares, we can infer that the number of trades per day is approximately 34 million. These are not spaced evenly throughout the trading day. There are often bursts of activity in one or many stocks, followed by stretches of relative inactivity. These bursts are often at speeds a human observer cannot keep pace with - frequently events are following within less than 1 millisecond of each other. Trading is also typically heavy in the closing auction: for NYSE-listed securities, for example, the closing auctions represent about 7% of the volume in those securities. 



<DownloadPageAsPdf/>
