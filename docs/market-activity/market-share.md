# Market Share 

The market share of a venue is calculated as the amount of trading volume that occurs on that venue on a given day, divided by the total volume across the US equities market. Thus, the sum of the market share over all venues will be equal to 1. Roughly 62% of trading volume happens on exchanges, with the remaining 38% happening elsewhere (dark pools or internalizers). Within the exchange landscape, each of the 3 major exchange families, CBOE, Nasdaq, and NYSE, represents about 20% of trading volume (summing up all of the individual exchanges within each family). The remaining 2.5% market share belongs to IEX. 


<DownloadPageAsPdf/>
