# Venues

There currently 3 large parent companies which each operate several stock exchanges. ICE operates five (this is often also referred to as the "NYSE family"): NYSE, NYSE Arca, NYSE American, NYSE National, and NYSE Chicago. CBOE operates four: EDGX, EDGA, BZX, and BYX. Nasdaq operates three: Nasdaq, BX, and PSX. The only exchange outside of the three families is IEX, which is operated as an independent company. 

There are roughly 30 ATS's, which is down from around 40 in 2014. 


<DownloadPageAsPdf/>
