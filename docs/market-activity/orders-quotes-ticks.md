# Orders, Quotes, and Ticks

The number of orders that are transmitted to a venue is typically much higher than the number of resulting trades. This can be seen, for example, in [the data that the SEC provides](https://www.sec.gov/marketstructure/datavis/ma_exchange_canceltotrade.html#.XPiiclNKhp8) concerning the ratio of order cancelations to trades at each exchange. 

The ratio of cancelations to trades varies widely by date and by venue, but something like 20/1 is somewhat typical. This gives us a general sense that there is on average a lot of nimble maneuvering of quotes around each trade. 

Over the course of a trading day across all listed securities, there are tens of millions of times when an NBO price or NBB price changes. NBO stands for "National Best Offer," and it refers to the lowest price advertised by a would-be seller across exchanges. NBB stands for the "National Best Bid," and it refers to the highest price advertised by a would-be buyer across exchanges. The "NBBO" is a shorthand often used to refer to the pair of the NBB and NBO prices. Naturally, the NBB/NBO are time-dependent constructs, and practically speaking they are also latency-dependent constructs, as information about price changes takes time to travel from one market participant to another. 

The number of NBBO changes per day is roughly on the same order of magnitude as the number of trades. Note that this does not count times when the total *size* available at the best bid or best offer price changes, but the prices themselves do not change. Nor does it count the times when displayed orders *away* from these best prices are changed or canceled. 

A fairly high percentage of NBBO price changes occur in rapid succession: nearly 40% of them occur within 1 millisecond of another NBBO price change. To put this in context, we can calculate the number of milliseconds (ms) in the trading day as: 6.5 hours x 60 minutes/hr x 60 seconds/minute x 1000 ms/s = 23,400,000. If we approximate the total number of NBBO price changes across all roughly 8300 listed securities as 20 million, then we have on average about 2400 NBBO price changes per trading day per stock. So this is about 1 NBBO price change every 10,000 milliseconds. So the fact that they often ocurr within 1 ms of each other reflects a rather extreme amount of clustering, suggesting that we should conceptualize price changes as isolated bursts of activity, punctuating long stretches of relative calm. 


<DownloadPageAsPdf/>
