# Birds-eye View of Overall Market Activity

## How Much and How Often

Now that we've fleshed out the different kinds of entities in the US equities market and the logistics of communications between them, it's time to zoom out a bit and get a sense of scale and proportions. How much trading activity happens on a typical day? How it is distributed by size and time and place? How much variation is there intraday and interday? Here we will give you some basic answers to these questions. Obviously, there is a lot more structure and patterns underlying trading than the first order statistics here can capture, but our goal for now is just to get you started on building a basic intuition for the scale and nature of market activity. 



<DownloadPageAsPdf/>





