# Volatility and Variation
Over time, there is a lot of variation in the kind of daily stats we have quoted above. Some variation in trading behavior is driven by structural things: e.g. index rebalancing or the fact that stock options have certain standardized expiration dates (following the third Friday of a given month). But even aside from these anticipated structural events, within a day or hour or minute, etc. there can be wild changes in activity and prices within a single stock, multiple stocks, or broadly across the market. The number of NBBO changes in a day for instance, can vary by a multiplicative factor of 3 or 4 (or more) when we compare a relatively calm trading day to a relatively turbulent one. 

In a technical sense, "volatility" in the context of stock trading usually refers to the standard deviation or variance of prices for a particular security or set of securities. Roughly, this is a measure of how far prices tend to drift above and below their average over a specified unit of time. This gives additional color around price trends that mere averages or medians do not. For example, if we say "the average price of Microsoft over the trading day today was X," we mean that we took all the trades of Microsoft over the day and averaged their prices (presumably weighted by their sizes). But this alone tells us nothing about whether we had, say, many trades at price exactly X, or trades that swung widely between lows of X/2 and highs of 2X, averaging out to X overall. Volatility disambiguates these disparate situations. Low volatility means that we had trades mostly clustered tightly around the average price, while high volatility means that prices oscillated considerably. 

Information about expected future volatility can be inferred by looking at option prices. Since the value of an option depends on the probability that a stock price will go above or go below a specified strike price in the future, the price of an option right now can tell us something about "the market's" estimate of the likelihood of particular future price movements. The CBOE Volatility Index, or VIX, measures this kind of "implied volatility" by using SPX option trades to infer the market's expection of 30-day volatility in the S&P 500 index. 

 

*Further reading*:

[SIFMA Insights: US equity Market Structure Primer, July 2018](https://www.sifma.org/resources/research/equity-market-structure-primer/) 
(*this was a source for many of the stats above)


<DownloadPageAsPdf/>
