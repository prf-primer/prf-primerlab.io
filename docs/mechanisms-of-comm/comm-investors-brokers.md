# Communication between Investors and Brokers

Once an investor has decided what they'd like to buy or sell, how much, and some goals in terms of price and/or timing, they need a way to communicate this to a broker who will manage the process of formulating individual orders, routing those orders to various trading venues, and keeping track of the trades that are produced. There is a lot of variety in exactly how granular the investor's instructions are and how they are communicated. Often the communication is accomplished or assisted through an OMS and/or EMS:

## EMS/OMS Providers

The current standard buy-side trading workflow incorporates certain core pieces of technology, including an order management system (OMS) and an execution management system (EMS). 

An OMS tracks the life cycle and status of orders across the firm, often across multiple asset classes. The "life cycle" of an order refers to all of the relevant events that can occur over the "lifetime" of an order: events like the order being placed at a venue, partially filled, modified, routed away to another venue, fully filled, or canceled, etc. 

An EMS is a tool that focuses on individual order execution and allows more fine-grained control, such as slicing a "parent" order into "child" pieces to be sent to a specific counterparty or venue. The EMS also generally has more context on what's happening in the market in real-time to allow the trader to make informed decisions.

A typical work flow is: a portfolio manager at an institutional investor decides to put on a position in US equities, creates an order in the OMS and assigns it to a trader. The trader then stages the order in their EMS which allows them to send pieces (child orders) externally to their brokers. As these child orders get executed at various trading venues, those fills (aka trades) flow back to the EMS, and subsequently to the OMS. The OMS will likely perform or enable various compliance and risk functions, although in some cases, the EMS might have some built-in risk check logic as well.

These two sets of functionalities are quite related and overlapping, and some vendors try to combine them into a single offering. Overall, the general quality of these technology products is unimpressive, and like many other services in this industry, their pricing models and specific offerings are extremely opaque. 

It's an extremely sticky business as the process to transition from one provider to another without losing any custom functionality is a painful one. 

_Further Reading_: 
<!-- IMPORTANT: put static assets in /docs/.vuepress/public/assets -->
[New Plateaus for OMS EMS Integration](/assets/New_Plateaus_for_OMS_EMS_Integration_White_Paper.pdf),
[EMS Consolidation](http://blog.alignment-systems.com/2015/10/ems-consolidation.html),
[Liquidnet Trading Solutions](https://www.liquidnet.com/equities-trading-solutions)

 

## High touch vs. low touch
Within a broker, there are different models for how investor orders are handled. One main axis of differentiation is "high touch" versus "low touch."

In this context, high touch refers to execution services managed by a human, whereas low touch refers to automated execution services, such as trading algorithms. High touch equities teams are sometimes called "cash trading" and low touch "electronic trading." High touch desks often provide additional services such as shopping orders to other clients to directly match a large amount (especially large trades are often called "blocks"), committing capital to take down some or all of the trade, and providing market color and commentary. 

### Roles on a high touch desk
A high touch desk will typically have employees organized into the following roles:

1. **Trader** - a person who directly manages the execution of orders.
2. **Sales-trader** - a hybrid trader role and sales role that performs duties like shopping flow.
3. **Sales** - account management and business development.
4. **Desk analyst** - a person who conducts independent research and provides more in-depth color than a salesperson or sales trader.

### Roles on a low touch desk
A low touch desk will typically have employees organized into the following roles:

1. **Sales-trader/sales** - same as the analogous roles above.
2. **Algo developer/product management/client implementation** - there are several front-office technology roles that work together to build the actual trading platform, manage customizations, and connect to customers and the street. 
3. **Operations/support** - the low touch desk's technology is often the backbone of the high touch desk, and when software/hardware/networking issues come up, people in these roles lead the effort to resolve them.

## Algo Customizations

One of the core features of most institutional trading platforms is the ability for individual clients to customize the behavior of their chosen trading algorithms. For example, a buy-side trader who is sending orders to be executed by a sell-side broker's electronic trading algorithm might have the option to specify that "the algo" (as it is often lovingly called) should exclude a subset of dark pools, and/or maintain a minimum percentage of volume, and/or always utilize a given minimum execution quantity. These customizations are generally configured or hard coded behind the scenes in the broker's system.

## Transaction Cost Analysis (TCA)

Buy-side investors may reasonably want to track and compare performance of brokers across many orders. They might use such data to inform decisions about what kinds of orders to give to which brokers, etc. Getting the relevant data in an actionable form can be challenging, as brokers are sometimes resistant to providing granular detail about their operations to the buy-side clients, and it is not an easy task to isolate the effects of broker decisions from the cummulative biases of data samples and broader market activity. Brokers providing their own analysis of their own trading is clearly problematic, as they are naturally incentivized to choose metrics and data sets that make them look good. 

This particular category of communication from brokers back to investors is not very standardized, and the results are often not very satisfying. Getting enough relevant data into a usable format alongside sufficiently granular market data in order to draw meaningful conclusions remains a non-trivial task. This is especially true for relatively smaller investors who have less resources and leverage to devote to this. As a result, several third-party providers have arisen to provide transaction cost analysis and help buy-side traders evaluate sell-side broker performance. For more discussion of transaction cost analysis and what it entails, see our related [blog post. ](https://medium.com/prooftrading/building-a-lightweight-tca-tool-from-scratch-proof-edition-6fd1c716eee0)


<DownloadPageAsPdf/>
