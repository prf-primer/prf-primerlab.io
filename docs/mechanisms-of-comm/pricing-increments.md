# Pricing Increments 
Trading in the US equities market is also confined to a discrete standard of pricing. Would-be buyers and sellers of securities on exchanges must advertise prices over $1 in whole pennies, and trades must be executed at whole penny increments, with the exception of trades at the "midpoint" of advertised buy and sell prices, which sometimes lands on a half-penny increment. The rules are a bit looser for dark pools and internalizers, or for trades that are consummated by participants themselves outside of venues, but there are still constraints of this spirit. 

The main goal of imposing a coarse discretization of price is to avoid participants snatching trades out of each other's hands by offering essentially meaningless improvement on price. For example, a would-be buyer who is willing to pay $10 a share for a stock being out-bid by someone willing to pay $10.0000001 a share for a stock is not very civilized. This sort of thing would undermine confidence in the health of the market system, and incentivize some rather silly behavior. Though this motivation of discretization is relatively clean and clear, there is a strange quirk of the way the rules for exchanges are currently written and enforced: participants often *trade* at half-penny increments on exchanges as mid-point trades, but can only *express interst to trade* at full-penny increments. 



<DownloadPageAsPdf/>
