# Mechanisms of Communication between Participants

## The When and the How
Now that we have laid out the different types of participants in the US equities market and what their goals might be, we'll move on to discussing the logistics of communications between them. Any full description of communication and interaction in a complex ecosystem should start with the core, established rules that participants all operate under and assume. These do not need to be actively communicated, but rather form the shared context underlying communications. In US equities trading, there are core rules concerning time and price. After discussing the basics of these, we'll go on to the discuss the logistics and content of active communications between the various participants. 



<DownloadPageAsPdf/>
