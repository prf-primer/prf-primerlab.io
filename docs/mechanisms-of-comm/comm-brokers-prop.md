# Communications among Brokers/Proprietary Traders

In the realm of retail trading, some communication happens directly between brokers and proprietary traders. In fact, most retail trading occurs through direct relationships between retail brokers and proprietary trading firms. The process typically works like this: a retail trader submits an order to a retail broker (e.g. Robinhood, ETrade, etc.) The retail broker sends it to a proprietary trading firm that is acting as a retail wholesaler. The proprietary trading firm decides whether they want to execute the trade with themselves as the counterparty or route it elsewhere. 

This is an attractive arrangement for the retail broker, who typically gets payment for order flow (PFOF) and/or price improvement from the retail wholesalers. In this context, "price improvement" means that the end customer (the retail trader) will get a slightly more favorable trade price than what market makers are currently advertising on the exchanges. The retail wholesaler is willing to do all of this (PFOF and/or price improvement) because retail traders typically have small orders (hence limited risk) and trade without significant short-term alpha. (This is a polite way of saying that on a short-term time scale, retail traders don't tend to buy low and sell high.) For this reason, market makers can make an excellent profit off of retail traders in aggregate, so they are willing to grant better prices and pay for order flow when they know they are trading against a less sophisticated counterparty. 


<DownloadPageAsPdf/>
