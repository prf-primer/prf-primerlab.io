# Regulatory Constraints and Surveillance
Here we will discuss the main regulations that serve as constraints on market activity, and the communications and data that regulators rely upon to enable their enforcement. These regulatory constraints should be thought of as part of the assumed common knowledge underlying market interactions. We did not discuss all of them above when we discussed the logistics of the trading day and discrete price increments only because some of them require a pretty deep in the weeds perspective to describe. Only now after we have discussed more of the details of communications and interactions between various participants do we have the semantic tools to summarize some of these regulations and their current effects on the market ecosystem. 

## Major Regulatory Constraints

In the wake of the stock market crash of 1929, new federal laws were passed with the intent of protecting investors and the integrity of markets. Prior to this, stock sales were regulated only under state laws. The goal of the Securities Act of 1933 was to set consistent standards for public companies to protect investors from fraud. The Securities Exchange Act of 1934 created the SEC (the Securities and Exchange Commission) as the regulator of the public markets and established the concept of self-regulation, making exchanges share responsibility for policing their own markets. 

Since then, many more regulations have been put in place with the intent of constraining potentially harmful trading activities, incentivizing healthy trading, and ensuring reliability and availability of market infrastructure. We'll address much of this in more detail in a market history section later, so for now we will just highlight the regulations that have a major impact on current trading mechanics. 

### Securities Act Amendments
In 1975, the securities act amendments were passed, creating a more organized and centralized system to connect exchanges together. The SIPs were created to be a central source of the most relevant information across disparate exchanges. 

### Reg NMS
In 2005, Regulation National Market System (aka Reg NMS) was introduced which established the Order Protection Rule, among other regulatory changes. The order protection rule mandates that trading venues cannot match trades at prices that are inferior to the best prices currently available on other venues. For example, if a market order to buy arrives at a venue V where the lowest limit a seller has set is $11.00 per share, but some other venue W has a displayed sell order available at $10.50 per share, the buyer and seller cannot be matched at venue V for $11.00 per share. The buy order must be routed to venue W instead to be matched with the seller there at the lower advertised price. 

Obviously this is intended to protect the buy order from being executed at a sub-optimal price, but there are a lot of devils hidden in the details. For instance, what does "currently available on other venues" technically mean? For venue V to fulfill its obligations under Reg NMS, it must now track the best available prices on venue W (and everywhere else). Since information takes time to travel from venue W to venue V, the view that venue V has of the "best prices currently available on venue W" is necessarily delayed. The amount of this delay is a dynamic function of all of the communication infrastructure between W and V. V is this regard is similar to any broker connecting to venue W, and it must choose how much to invest in reducing latency.  

### Reg SHO
Regulation SHO concerns the practice of short-selling, which is selling a stock without owning it. Reg SHO concerns "locate" and "close out" requirements which try to assure that stocks which are sold by short sellers can be reliably borrowed and delivered on time. Reg SHO also puts limits on short selling in the midst of a substantial intraday price decrease. This is intended to address concerns that short-selling can be used manipulatively to artifically depress the price of a stock. 

### Reg SCI
Regulation Systems Compliance and Integrity (or Reg SCI as it is commonly called) was established in 2014 with the goal of reducing the likelihood of significant to catastrophic technology failures in the financial markets. This regulation created more formal requirements for the operations and testing of core components of trading technology and applies broadly to venues and clearing agents.  

### LULD bands and circuit breakers
Limit Up Limit Down bands (LULD) prescribe a certain price window that a stock can trade in, as a function of the stock's prices over a preceding five minute window. The bands ensure that a price can't increase or decrease too suddenly by too large of a percentage. Trades outside the band are prohibited, and if prices do not revert back into the band within 15 seconds, a 5-minute trading pause in the stock is triggered. 

While LULD bands function individually for each stock, there are also market-wide circuit breakers, which are triggered when the S&P Index decreases by certain percentages, as compared to the previous day's closing price. There are three levels at which circuit breakers can be triggered: Level 1 is triggered by a 7% relative decrease, and will halt trading across the market for 15 minutes (though only if it is before 3:25 pm). Level 2 is triggered by a 13% relative decrease and will also halt trading broadly for 15 minutes (again only before 3:25 pm). Level 3 is triggered by a 20% relative decrease, and halts trading across the market for the remainder of the day, no matter the time. 

## Logistics of Regulatory Surveillance 
To monitor for compliance with regulations, to investigate potential cases of market manipulation, and to more generally keep a finger on the pulse of the markets, regulators needs access to granular data about trading mechanics and behaviors. Here we discuss some of the primary data sources that FINRA and the SEC collect from market participants to enable regulatory surveillance. 

### 605/606 reports
SEC Rules 605 and 606 require venues and brokers to make periodic public disclosures of basic aggregate information about executions. Rule 605 mandates that venues make monthly reports of basic execution quality metrics on publicly accessible websites. Rule 606 mandates that brokers disclose information about which venues they route orders to and in what proportions.  

### OATS
OATS stands for the "Order Audit Trail System" that is used by FINRA. It was established by FINRA rules in 1998 (approved by the SEC). All FINRA member firms are required to collect and submit the data according to the technical specficiations of OATS, and FINRA uses the data for market surveillance. The data spans orders, quotes, and trades for listed stocks as well as over-the-counter securities (securities that are not listed on a stock exchange). In aggregate, this data is intended to allow the re-creation of order lifecycles, so that the chain of events from order entry to order modification or cancellation or execution can be followed. 
 
### CAT
In 2012, the SEC called for the creation of a "Consolidated Audit Trail" (CAT) to serve as a key data source for market surveillance and regulatory investigation. This is intended to replace OATS and to enable superior surveillance capabilities. In the longterm, all venues and brokers will report data into the CAT processor in prescribed format. The CAT processor will be responsible for assembling this data into easily query-able form and linking together the different pieces of an order life-cycle, so events like order submission, modification, routing, execution, and cancellation can be stitched together and in some cases attributed to the originating investor. Granular identifiers of venues, brokers, and end customers will be maintained so that ultimately regulators can identify actors in involved in any questionable activity. Regulators such as the SEC and FINRA will be given access to the CAT in prescribed ways to enable market surveillance. 

The CAT is intended to represent a more complete and sophisticated tool for regulators (particularly at FINRA and the SEC) to get effective information about behaviors in the marketplace. However, it is a complex project with many different stakeholders who are often at odds about exactly what data should be collected, how quickly reporters should have to adapt their systems to report such data, and who should have access to what. Long delays and disagreements have plagued the CAT since its inception. 



<DownloadPageAsPdf/>
