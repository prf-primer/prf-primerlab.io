# History of the stock market/market structure


[Develop in the context of competing motivations/goals among the different participants, and evolving balance between them. This will lead nicely into the next section on controversies.]

Key points/trends in time to cover:
- Exchange Act
- creation of the SIPs
- creation of Nasdaq
- creation of BATS, direct edge, etc. 
- floor trading replaced by electronic trading
- FINRA - history of its role?
- Reg NMS
- Finra rule 5310 on best ex
- consolidation into 3 exchange families
- rise of dark pools as taking on significant volume
- Dodd-Frank (fee filings becoming immediately effective)

