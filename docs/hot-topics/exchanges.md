# Exchanges


## Monopolistic practices/price gouging

### Prop market data feeds

Allison's blog post part 1 - [https://medium.com/prooftrading/market-data-pricing-part-1-of-many-f4213ac214f6](https://medium.com/prooftrading/market-data-pricing-part-1-of-many-f4213ac214f6)

Adrian's paper - [https://iextrading.com/docs/The Cost of Exchange Services.pdf](https://iextrading.com/docs/The%20Cost%20of%20Exchange%20Services.pdf)

### Connectivity

### Co-location

### Pricing tiers

[https://www.nytimes.com/2016/03/02/business/dealbook/stock-exchange-prices-grow-so-convoluted-even-traders-are-confused-study-finds.html](https://www.nytimes.com/2016/03/02/business/dealbook/stock-exchange-prices-grow-so-convoluted-even-traders-are-confused-study-finds.html)

### Listings? Spotify IPO


## Conflicts of interest

### Operating the SIPs

### For-profit SRO


## Complex order types


## SEC fines

NYSE fined $14mm for July 2015 outage - [https://nypost.com/2018/03/06/new-york-stock-exchange-fined-14m-for-2015-outage/](https://nypost.com/2018/03/06/new-york-stock-exchange-fined-14m-for-2015-outage/)
