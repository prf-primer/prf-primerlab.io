# Market makers, prop firms


## Predatory trading


## Wasteful technology investment 
(e.g. "racing")


## KCG collapse 
(maybe a whole page of major technology outages/failures?)


## BATS failed IPO


## NYSE July 2015 3.5 hour outage 
(July 2015)


## Nasdaq FB IPO

[Nasdaq UTP SIP 15 min outage (January 2013)](https://www.reuters.com/article/exchanges-data-outage/data-feed-glitch-leaves-some-traders-in-dark-on-nasdaq-prices-idUSL1E9C3DQL20130103)

[Nasdaq SIP outage/total shut down for 3 hours (August 2013)](https://www.reuters.com/article/us-nasdaq-halt-tapec/nasdaq-market-paralyzed-by-three-hour-shutdown-idUSBRE97L0V420130822)

[NYSE CTA SIP 27 min outage (October 2014)](https://blog.themistrading.com/2014/10/sip-crash-the-sequel/)

## Other exchange outages

