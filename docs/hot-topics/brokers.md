# Brokers


## Conflicts of interest

### Rebate harvesting / non-passthrough pricing

### Operating a dark pool

### Trading against customers / central risk book?

### IPO underpricing

### TCA 
(i.e. grading their own report card)

### (Retail) Payment for order flow


## SEC fines 
(maybe have a full page describing SEC fines of market participants - and link to those sections here?)

### Misleading customers

#### In marketing materials 
(e.g. Barclays)

#### BAML passing through the wrong LastMarket tag

### Insufficient risk checks

https://www.wsj.com/amp/articles/wall-streets-dark-pools-face-new-transparency-rules-1531924509

[https://www.sec.gov/news/press-release/2018-256](https://www.sec.gov/news/press-release/2018-256)

[https://www.wsj.com/articles/credit-suisse-barclays-to-pay-about-150-million-to-settle-dark-pool-investigations-1454256877](https://www.wsj.com/articles/credit-suisse-barclays-to-pay-about-150-million-to-settle-dark-pool-investigations-1454256877)

ITG secret trading desk, misusing dark pool info $20.3mm - [https://www.sec.gov/news/pressrelease/2015-164.html](https://www.sec.gov/news/pressrelease/2015-164.html)

Pipeline $1mm Oct 2011 [https://www.sec.gov/news/press/2011/2011-220.htm](https://www.sec.gov/news/press/2011/2011-220.htm)

Merrill Lynch (BAML) $42mm for misleading customers about trading venues June 2018 [https://www.sec.gov/news/press-release/2018-108](https://www.sec.gov/news/press-release/2018-108)
