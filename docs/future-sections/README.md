# History of the Stock Market

Key points/trends in time to cover:
- Exchange Act
- creation of the SIPs
- creation of Nasdaq
- creation of BATS, Direct Edge, etc. 
- floor trading replaced by electronic trading
- FINRA - history of its role?
- Reg NMS
- Finra rule 5310 on best ex
- consolidation into 3 exchange families
- rise of dark pools as taking on significant volume
- Dodd-Frank (fee filings becoming immediately effective)

# Current Issues

Conflicts of interests between participants and controversial practices:

- Brokers: conflicts of interest vs. their clients, bundling of services, SEC fines
- Exchanges: conflicts of interest vs. brokers and vs. investors, monopolistic practices
- Investors: fraud, ponzi schemes, concerns about (manipulative?) short-selling
- Issuers: stock buy-backs, issues around IPO pricing
- Market makers and proprietary trading firms: predatory trading practices, wasteful technology investment
- Regulators and academics: revolving door between regulators and the regulated, academic studies funded by stakeholders, data quality/availability issues
- Everyone: technology failures/outages





# Proposed Fixes

some implemented or partially implemented:
- speed bumps
- Reg SCI
- CAT
- increased disclosure requirements

some theoretical:
 - frequent batch auctions
 - SIP upgrades
 - even more increased disclosure requirements
 - MFID II 

