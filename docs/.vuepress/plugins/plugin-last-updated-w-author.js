const spawn = require('cross-spawn')

module.exports = (options = {}, context) => ({
  extendPageData($page) {
    const { transformer } = options
    const {
      committerDate: timestamp, 
      committerName: author
    } = getGitCommitDateAndAuthor($page._filePath)
    const $lang = $page._computed.$lang
    const commitAuthor = author || 'Unknown'
    $page.lastUpdatedBy = commitAuthor
    if (timestamp) {
      const lastUpdated = typeof transformer === 'function'
        ? transformer(timestamp, $lang)
        : defaultTransformer(timestamp, $lang)
      $page.lastUpdated = `${lastUpdated} by ${commitAuthor}`
    }
  }
})

function defaultTransformer(timestamp, lang) {
  return new Date(timestamp).toLocaleString(lang)
}

function getGitCommitDateAndAuthor(filePath) {
  let committerDate, committerName
  try {
    // https://git-scm.com/docs/git-log
    committerDate = parseInt(spawn.sync('git', ['log', '-1', '--format=%ct', filePath]).stdout.toString('utf-8')) * 1000
    committerName = spawn.sync('git', ['log', '-1', '--format=%cn', filePath]).stdout.toString('utf-8')
  } catch (e) { /* do not handle for now */ }
  return {
    committerDate,
    committerName,
  }
}