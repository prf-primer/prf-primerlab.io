const CONFIG = require('../../config.json')
const { stringifyIndex } = require('../../util')

const { GA_ID, PDF_BASE } = CONFIG
// https://vuepress.vuejs.org/guide/deploy.html#gitlab-pages-and-gitlab-ci
// https://vuepress.vuejs.org/config/#config-reference


const arrNavSections = [
  { 
    text: 'Download Full Document', 
    items: [
      { text: 'Market Structure Primer', link: `${PDF_BASE}/Proof-Market-Structure-Primer.pdf`}
    ]
  }, { 
    text: 'Download Sections', 
    items: [
      // { text: 'Introduction', link: `${PDF_BASE}/section_01_introduction.pdf`},
      { text: 'Life Cycle of an Order', link: `${PDF_BASE}/section_02_lifecycle-of-order.pdf` },
      { text: 'Market Participants', link: `${PDF_BASE}/section_03_participants.pdf` },
      { text: 'Mechanisms of Communication between Participants', link: `${PDF_BASE}/section_04_mechanisms-of-comm.pdf` },
      { text: 'Birds-eye View of Overall Market Activity', link: `${PDF_BASE}/section_05_market-activity.pdf` },
      { text: 'External Resources ', link: `${PDF_BASE}/section_06_external-resources.pdf` },
      // { text: 'Future Section', link: `${PDF_BASE}/section_07_history-of-stock-market.pdf`},
      // { text: 'Future Section', link: `${PDF_BASE}/section_07_hot-topics.pdf`},
      // { text: 'Future Section', link: `${PDF_BASE}/section_07_proposed-fixes.pdf`},
      // { text: 'Future Section', link: `${PDF_BASE}/section_07_glossary.pdf`},
    ]
  },
]


const sortedPagePaths = CONFIG.SIDEBAR.reduce((prev, sbGroup)=>{
  const { children, title } = sbGroup
  if (Array.isArray(children)){
    children.forEach(page => {
      const pagePath = page[0]
      prev.push(pagePath)
    })
  }
  return prev
}, [
  '/' // path for home page
])

const sortedPageMap = CONFIG.SIDEBAR.reduce((prev, sbGroup, sbGroupIndex)=>{
  const { children, title } = sbGroup
  if (Array.isArray(children)){
    children.forEach((page, pageIndex) => {
      const pagePath = page[0]
      prev[pagePath] = `${stringifyIndex(sbGroupIndex+1)}.${stringifyIndex(pageIndex)}`
    })
  }
  return prev
}, {
  '/': '00.00' // index for home page
})

module.exports = {
  title: CONFIG.SITE.TITLE,
  description: CONFIG.SITE.DESCRIPTION,
  head: [
    ['link', { rel: 'icon', href: CONFIG.SITE.FAV_ICON }]
  ],
  base:'/',
  dest: 'public',
  port: 8080,
  themeConfig: { 
    logo: '/assets/img/logo_B.png',
    
    // ---------- S I D E  B A R --------------
    // https://vuepress.vuejs.org/default-theme-config/#sidebar
    // sidebar: 'auto', // enable sidebar for all pages
    sidebarDepth: 2,
    sidebar: CONFIG.SIDEBAR,
    displayAllHeaders: false,
    activeHeaderLinks: false, // Default: true
    lastUpdated: 'Last Updated', // string | boolean
    
    // ---------- N A V  B A R --------------
    // https://vuepress.vuejs.org/default-theme-config/#navbar
    nav: [
      {
        text: 'Download Primer',
        items: arrNavSections,      
      // }, {
      //   text: 'Other Documentations',
      //   items: [
      //     { 
      //       text: 'Documentations Home',
      //       link: 'https://docs.prooftrading.com/' 
      //     }, { 
      //       text: 'Example Documentation',
      //       link: 'https://docs.prooftrading.com/example-doc/' 
      //     }, { 
      //       text: 'All About Bourbon',
      //       link: 'https://bourbon.prooftrading.com' 
      //     },
      //   ]
      }, {
        text: 'prooftrading.com',
        link: 'https://prooftrading.com'
      }, {
        text: 'Contribute!',
        link: 'mailto:primer@prooftrading.com'
      }
    ],

    // ---------- R E P O  L I N K S --------------
    // https://vuepress.vuejs.org/default-theme-config/#git-repo-and-edit-links
    // repo: 'https://gitlab.com/prf-primer/prf-primer.gitlab.io/docs/', // Assumes GitHub. Can also be a full GitLab url.
    //--- Customising the header label
    // repoLabel: 'Contribute!', // Defaults to "GitHub"/"GitLab"/"Bitbucket" depending on `themeConfig.repo`
    //--- Optional options for generating "Edit this page" link    
    docsDir: 'docs', // if your docs are not at the root of the repo
    docsBranch: 'master', // if your docs are in a specific branch (defaults to 'master')
    editLinks: false,  // defaults to false, set to true to enable
    editLinkText: 'Help us improve this page!', // custom text for edit link. Defaults to "Edit this page"
  },
  // ---------- P L U G I N S --------------
  // NOTE: only available in 1.x
  // https://v1.vuepress.vuejs.org/plugin/official/plugin-active-header-links.html
  plugins: [
    ['vuepress-plugin-export', { 
      sortedPageMap 
    }],
    ['@vuepress/back-to-top', {}],
    ['@vuepress/active-header-links', {
      sidebarLinkSelector: '.sidebar-link',
      headerAnchorSelector: '.header-anchor',
      headerTopOffset: 320,
    }],
    ['@vuepress/google-analytics', {
      'ga': GA_ID
    }],
    [require('./plugins/download-page-as-pdf'), { 
      sortedPageMap,
      hostPdfAssetsBase: PDF_BASE,
    }],
    [require('./plugins/plugin-last-updated-w-author')],
  ],
  
  // ---------- M A R K D O W N --------------
  // VuePress uses markdown-it as the markdown renderer.  A lot of the extensions are implemented via custom plugins.
  markdown:{
    toc: { // https://github.com/Oktavilla/markdown-it-table-of-contents
      includeLevel: [2, 3, 4, 5, 6]
    },
    anchor: {
      permalinkBefore: false, // Place the permalink before the title.	
      permalink: true, // Whether to add permalinks next to titles.	
      permalinkSymbol: '#', // The symbol in the permalink anchor.	
      level: 1, // Minimum level to apply anchors on or array of selected levels.	
    },
    config: md => {
      // list of markdown syntax extensions: https://github.com/markdown-it/markdown-it#syntax-extensions
      md.use(require('markdown-it-footnote'))
      md.use(require('markdown-it-abbr'))
      md.use(require('markdown-it-anchor'))
      md.use(require('markdown-it-task-lists'))
      // md.use(require('markdown-it-table-of-contents'))
    },
  }
}

