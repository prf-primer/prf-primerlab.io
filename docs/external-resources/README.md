---
next: false
---

# External Resources

[100 Years of Market Structure Improvement (KCG 2016, Page 27)](https://www.decarleytrading.com/images/stories/Technically-Speaking-November-2016.pdf)

[The New Stock Market: Sense and Nonsense (Fox, Glosten, Rauterberg 2015)](http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2580002)

[The Hidden Alpha in Equity Trading (Oliver Wyman 2014)](https://www.oliverwyman.com/our-expertise/insights/2014/mar/the-hidden-alpha-in-equity-trading.html)

[Will the Market Fix the Market? A Theory of Stock Exchange Competition and Innovation (Budish, Lee, Shim 2019)](https://faculty.chicagobooth.edu/eric.budish/research/Stock-Exchange-Competition.pdf)

[Equity Market Structure Primer (Sifma 2018)](https://www.sifma.org/resources/research/equity-market-structure-primer/)

[Yardeni Research US Flow of Funds: Equities (2019)](https://www.yardeni.com/pub/fofusequity.pdf)

[Incentivizing Trading Behavior Through Market Design (Aisen 2017)](https://iextrading.com/docs/Incentivizing%20Trading%20Behavior.pdf)


<DownloadPageAsPdf/>
