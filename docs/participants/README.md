# Market Participants

<div class="container-all-participant-banners" style="">
  <img style="margin: 0 20px 30px 0;" src="/assets/img/market-participants/banner_issuers.png">
  <img style="margin: 0 20px 30px 0;" src="/assets/img/market-participants/banner_underwriters.png">
  <img style="margin: 0 20px 30px 0;" src="/assets/img/market-participants/banner_investors.png">
  <img style="margin: 0 20px 30px 0;" src="/assets/img/market-participants/banner_venues.png">
  <img style="margin: 0 20px 30px 0;" src="/assets/img/market-participants/banner_brokers.png">
  <img style="margin: 0 20px 30px 0;" src="/assets/img/market-participants/banner_prop.png">
  <img style="margin: 0 20px 30px 0;" src="/assets/img/market-participants/banner_vendors.png">
  <img style="margin: 0 20px 30px 0;" src="/assets/img/market-participants/banner_clearing.png">
  <img style="margin: 0 20px 30px 0;" src="/assets/img/market-participants/banner_regulators.png">
</div>

## The Who and the Why

There are many different types of participants in the US equities market, each with distinct needs and objectives. Here we will focus on participants and trading mechanics involved in institutional trading. We will define our unit of a "participant" in terms of a single role, which does not always correspond smoothly to the often-used unit of a company. Within a large financial firm, there may be separate lines of business that fulfill separate roles, and treating the resulting mesh of sometimes conflicting incentives and functions as a "participant" can obscure the underlying mechanics driving behavior in the financial system. Conversely, considering only individual roles neglects the higher level constraints of company ties and relationships that can be needed to make sense of the lower level behaviors. To avoid both sources of potential confusion, we'll define and discuss individual roles as our base units first, and then periodically zoom out a bit to discuss the inter- and intra-company relationships between roles that comprise the overall financial ecosystem. 



<DownloadPageAsPdf/>
