# Clearing / Back Office
![banner issuers](/assets/img/market-participants/banner_clearing.png)

After a buyer and a seller are matched on a trading venue, there are back office processes that need to be done. The buyer needs to pay the seller, and the seller needs to produce the security. The security's title needs to be formally changed so that it now demonstrably belongs to the buyer. The entity who performs these back office processes is called a clearing broker. The clearing broker charges a fee for the services it provides, and manages regulatory compliance with this part of the trade. The goal of a entity performing a clearing service may be to maximize revenue from this service, or it might be to attract customers that it can then pitch on other services offered by the same firm. 



<DownloadPageAsPdf/>
