# Technology Vendors 
![banner issuers](/assets/img/market-participants/banner_vendors.png)

Technology vendors provide services that are useful (sometimes arguably required) for other participants. Technology vendors may provide physical and/or logical connectivity to trading venues, software that is used to communicate and/or track orders and trades, processed data feeds that contain various kinds of quote and trade information, access to microwave towers or fiber optic cables that carry messages quickly between market participants, cloud computing resources, database and analytics tools, etc. 

The goal of a technology vendor is typically to maximize its revenue in the long-term, but this may be consistent with low pricing in the short-term to encourage broad adoption and entrench dependency on their services. Especially for technology that connects participants, like execution management systems (EMS) that connect investors to brokers, it can be hard for participants to switch providers, as this has to be coordinated by both sides of the connection. These kind of effects dampen the power of competition among technology providers to drive prices lower and service quality higher. 

A technology provider might be in a strongly unique position to offer a particular service if it sits inside a company such as an exchange. For example, NYSE is clearly in the best position to offer optimally fast connection services into a NYSE exchange. Similarly, it is in the best position to offer up-to-date data feeds about what is going on at a NYSE exchange. These natural monopolies of latency-sensitive products and services that orginate at a trading venue are another phenomenon that heavily limits the power of competition to control pricing of services in the securities trading ecosystem.   



<DownloadPageAsPdf/>
