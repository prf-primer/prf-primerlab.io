# Underwriters
![banner issuers](/assets/img/market-participants/banner_underwriters.png)

When an issuer plans an IPO, it typically hires underwriters to be responsible for selling the initially offered stock. Underwriters typically make some kind of committment to sell a particular amount of stock at the IPO price. Often the total offering is split among several underwriters, who collectively form a syndicate. The immediate goal of an underwriter is to sell the initial stock and collect the corresponding fees. A more long-term goal is likely to position themselves for future underwriting business. Since underwriters are typically attached to firms that offer other financial services, they may have additional goals like pleasing the firm's other clients by offering them access to valuable IPOs. 



<DownloadPageAsPdf/>
