# Regulators
![banner issuers](/assets/img/market-participants/banner_regulators.png)

Regulators are charged with making sure that the stock market functions in an orderly fashion, and serves its intended purposes. Regulators create and update rules intended to protect the health of the market, and are charged with enforcing these rules. Regulators also must scrutinize and approve or disapprove changes that exchanges propose to their existing policies. The goal of regulators is to ensure that the rules are designed to serve the interests of investors and the market as a whole, and that the day-to-day activities in the market follow the rules. 

## SEC
The primary regulator of the US equities market is the SEC, which is a government agency. The SEC's mission is two-fold: investor protection and the facilitation of capital formation. The SEC is tasked with, among other things, policing investment firms, brokers, exchanges, and corporate issuers. The SEC is empowered to make policy that affects the operations of market participants. Sometimes the SEC will test the impacts of new policies before making them widespread or permanent in the form of "pilots." Pilots are policy changes that are applied in a way that is limited in time or in scope, typically designed to yield a meangingful comparison with market behavior happening in parallel that is not subject to the contained changes. 

The SEC is also empowered to review and to potentially reject changes to operations that trading venues like exchanges would like to make. The goal here is to ensure that proposed changes are not counter to the interests of investors or the facilitation of capital formation. This can be a difficult job, as there is often little or no quantitative basis to understand what impact the proposed changes might have until they are implemented. 


## FINRA
FINRA (the Financial Industry Regulatory Authority) is a private not-for-profit organization that oversees the broker-dealer industry. Like the exchanges, it is a self regulatory organization and thus has power to create and enforce rules and impose fines on its members.

As per their [website](http://www.finra.org/about/what-we-do), FINRA does five things:

1. Deter misconduct by enforcing the rules
2. Discipline those who break the rules
3. Detect and prevent wrongdoing in the US markets
4. Educate and inform investors
5. Resolve securities disputes



<DownloadPageAsPdf/>
