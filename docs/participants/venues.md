# Trading Venues
![banner issuers](/assets/img/market-participants/banner_venues.png)

Trading venues are the entities who match orders to buy and sell stock, and execute the resulting trades. A more common phrasing for that would be: "trading venues are where buyers and sellers find each other, and trades take place." We have deliberately eschewed this common phrasing, as it obscures the active role of the trading venue as an entity with its own goals. Despite the name of "venue," it can be misleading to think of a trading venue as a "place." A trading venue is still a "who."

A trading venue's goal might be to increase its revenue from trading fees or other fees that it charges to those who use its services. A related goal may be to attract as many buyers and sellers as it can in order to execute more trades. Another goal may be for the trading it executes to exhibit certain desirable features. To try to accomplish these goals, a trading venue has many tools at its disposal. It can design/modify its rules for how buyers and sellers express their interest to buy and sell semantically. It can design/modify its infrastructure for how buyers and sellers communicate with it mechanically. It can design/modify its rules for how compatible interests are ultimately matched into trades. It can also design/modify its rules for who can use its services, and what information about active interest and completed trades is provided to whom, up to some regulatory constraints. The constraints differ based on the type of venue, and from a regulartory perspective there are two types of venues:


## Exchanges

Exchanges are heavily regulated by the SEC and are also self-regulatory organizations (SROs) themselves - this means that the exchanges have certain regulatory authority over their customers. Exchanges generally publish firm quotes describing at what prices market participants are currently willing to buy or sell a given stock in real-time. In addition, exchanges publish messages describing the price and quantity of transactions after they're consummated. These trade and quote messages are known as market data. 

In some markets around the world, certain securities trade exclusively on a single exchange, but in the US, pretty much all public companies are traded on all venues. 

There are currently 3 major exchange families: ICE (NYSE) which operates 5 exchanges, Nasdaq which operates 3, and Cboe (BATS) which operates 4. IEX is the only other current exchange operator, though there are three other companies with exchange plans in the works: MEMX, MIAX, and LTSE. 


## ATS's 

Alternative Trading Systems (aka ATS's) are much more loosely regulated than exchanges and hence have lower barriers to entry. They typically don't disseminate quotes. ATS's come in two flavors:

### Dark Pools

Dark pools are designed to allow many would-be buyers to interact with many would-be sellers. They function pretty much like exchanges, but with a few major differences. Since they are scrutinized in a more lenient regularatory framework, dark pool operators can pick and choose which participants they allow to use their service. (Exchanges in contrast must provide fair access to any entity that follows the rules.) Dark pools also tend to tightly control information, not revealing who their subscribers are or any details about the trading in their pools that is not required for them to reveal. They do report trades after they happen - something we will discuss in more detail later after we have defined the mechanics of communication among the various players.  

The typical explanation for why dark pools exist is that "investors with big orders to buy/sell want to trade quietly in a semi-private pool with other investors, away from the revealing lights of exchanges which publish more information for all the sharks to see." But it is important to separate the goals of the investors and traders who might use a dark pool from the goals of the dark pool itself. The dark pool operator may have similar goals to an exchange operator: maximizing revenue from trading and other fees, maximizing trading activity, obtaining certain features of trading, etc. 

Dark pools must be operated by broker-dealers, and they are typically run by large banks. The largest dark pools are run by UBS, CS, JPM, GS, Barclays, MS, and Level (owned by a consortium of banks). This introduces other possible goals a dark pool operator might have that don't necessarily make sense for the dark pool in insolation, but do make sense in the context of the operator's larger business. 

### Internalizers

Internalizers are designed to allow many would-be buyers/sellers to interact with a single possible counterparty. These are crossing engines operated by proprietary trading firms and banks, where the operator trades against the client. Most retail trading occurs in this fashion, and a portion of institutional trading executes through these channels as well. The largest internalizers are Citadel and Virtu. 

The operator of an internalizer might have it as a goal to execute more of their own trading internally so that they minimize their fees to exchanges and other venues.





<DownloadPageAsPdf/>
