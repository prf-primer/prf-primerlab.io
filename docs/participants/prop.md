# Proprietary Traders
![banner issuers](/assets/img/market-participants/banner_prop.png)

A large segment of the trading ecosystem is proprietary trading firms - companies that use their own capital to buy and sell securities directly in the market. By most estimates, these firms comprise roughly 50% of all trading volume in the US. Some of the largest firms in this segment are Citadel, Virtu, HRT, Tower Research, IMC, TwoSigma, and Quantlab.

In terms of goals, it's not clear why someone trading with their own money should behave any differently than someone trading on behalf of others. Growing capital and managing risk etc., are reasonable goals for either case. However, there is a general differentiation in terms of time horizons. At least most (if not all) of the large proprietary trading firms focus on time horizons within a day or even shorter time intervals, and do not seek to hold positions overnight. 

At short time horizons, one common goal is market making, which means connecting buyers and sellers who are separated by time and/or venues, and charging a fee for the service. Market makers may stand ready to buy or sell a stock at any moment, with the price they are willing to buy at being slightly lower than the price they are willing to sell at. The difference between these prices is called the "spread," and it is the compensation that market makers get for providing immediate transactions to buyers and sellers who might otherwise have to wait seconds, minutes, or hours to find each other in the larger market, if at all. 

A proprietary trader might also have a goal of expressing a very short-term opinion about the price of a security. The time horizon here might be as short as 1 millisecond or less. Such opinions can be formed on the basis of very fast data feeds and recognition of common patterns in how price changes tend to travel throughout the fragmented landscape of venues. Some high frequency trading strategies seek to leverage predictive analytics acting on top of expensive high-speed data access at various venues and high-performance architecture to make a profit off of patterns of trading at a millisecond or microsecond time scale. Market makers may also employ such technologies to try to adjust their buying and selling prices ahead of detectable trends at this time scale. 



<DownloadPageAsPdf/>
