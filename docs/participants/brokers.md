# Brokers
![banner issuers](/assets/img/market-participants/banner_brokers.png)

A broker is a firm that facilitates trading on behalf of an investor. The brokers are collectively referred to as the "sell-side." In the US equities market, only broker-dealers are allowed to participate directly on an exchange. One goal of a broker is presumably to provide a service to the investor that meets the investor's needs, at a cost which is profitable to the broker. 

The mechanics of the relationship between a broker and an investor can take on many different forms, but a good rule of thumb to keep in mind is that an investor decides what to trade and when to trade at a coarse level, while a broker decides when to trade at a more granular level, as well as where to trade and how to trade. For example, an investor may convey to a broker that they want to buy 10,000 shares of Microsoft sometime over the next few hours. The broker will decide if/how to break this amount into smaller orders and how to put these orders into trading venues, potentially spreading them out over venues and over time. The investor will naturally want the broker's decisions to result in purchasing the target amount of stock within the target time frame at the lowest possible cummulative price. Competition among brokers is supposed to drive brokers to achieve this goal while charging competitive fees for their services. 

There can be complicating tensions, however, between the goals of the investor and the goals of the broker that arise from the ways that brokers charge for their services, which may holistically encompass more than just trading. To achieve goals like attracting business and maintaining/increasing profitability, brokers can do more than just tinker with their trading algorithms. They can also design and change their billing practices, as well as their full offering of services.  


## Commissions

Unlike many other businesses, brokers often charge different commission rates to their customers, even for the same type of execution. In US equities, broker commissions are usually charged on a flat per-share basis, or on a simple scale based on the securities price (for example, 1 cent per share on stocks below $5; 3 cents per share on stocks above $5). Common equities commissions range from free to 5 cents per share (an enormous range), with a typical rate in the ballpark of 0.5 cents per share.


## Cost Plus

Many brokers offer their clients the option of cost plus pricing, which means charging a relatively small commission and passing through their actual explicit transaction costs. For example, if a broker charged "cost plus 10 mils," this would mean they tally up the exchange and dark pool fees (and rebates) that they incurred while executing this order, and then add a flat 10 mils per share on top of this. (A "mil" is a one-hundredth of a penny, for some cruel reason, despite the usual rule that "milli" as a measurement prefix corresponds to a one-thousandth, e.g. there are 1000 milliseconds in a second. But I digress.) Cost plus pricing has the wonderful benefit of removing the incentive for a broker to route (potentially to the client's detriment) based on its own economic outcome, but it complicates the billing process and many investment firms are unable to process cost plus billing. Without cost plus pricing, brokers are incentivized to route to venues that charge lower fees or give them rebates, which may be in conflict with getting the best trade price for clients. 



## Researchers and Corporate Access

Full service brokers also provide services like research and corporate access. Researchers produce detailed research reports on companies and sectors. This role exists at companies called "full service brokers," which provide a wide array of services to instituational investors beyond trade execution. Full service brokers also coordinate direct access to corporate executives for interview. 

In the US, brokers generally don't charge fees for these additional services, and instead they expect to receive indirect compensation via their trading commissions. Those payments are thus considered part of the transaction costs as opposed to operational costs for their investment firm clients, an arrangement which many buy-side firms prefer. This means the typical goal of providing these additional services is to acquire or at least maintain trading business. This leads to some trading behaviors that seem weird in isolation but can be explained in the larger context. For example, an institutional firm may receive comparatively worse trading services for higher commissions, and still be unwilling to move too much of their business to another broker, because they value the research and other services provided by their full service broker. 


## Prime brokerage
There are other services that can be bundled together in the offerings a broker makes to clients like hedge funds. For example, a hedge fund who uses multiple brokers may not want to track and manage all of their trading relationships and collateral requirements individually. A prime broker can serve as a centralized manager of their activity to reduce the overhead for the client. 

## Stock loan
Brokers can also facilitate stock lending and borrowing for their clients. This can allow clients to sell a stock "short," meaning that they don't own the stock they are selling, but rather have borrowed it, with an obligation to return it at a later point in time. Short sellers are often expecting the price of a stock to go down, and can profit if it does, since they can buy the stock to return at the later time in the market for a lower price than what they previously sold. Brokers may manage the lending and borrowing of stocks for their clients, and may set collateral requirements for securing the loans, etc. 

*Further reading*

[https://en.wikipedia.org/wiki/Prime_brokerage](https://en.wikipedia.org/wiki/Prime_brokerage)

[https://en.wikipedia.org/wiki/Securities_lending](https://en.wikipedia.org/wiki/Securities_lending)



<DownloadPageAsPdf/>
